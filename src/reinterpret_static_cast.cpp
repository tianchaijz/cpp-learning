// http://stackoverflow.com/questions/15578935/proper-way-of-casting-pointer-types

#include <iostream>

template <typename T> void print_pointer(const volatile T *ptr) {
  // this is needed by oversight in the standard
  std::cout << static_cast<void *>(const_cast<T *>(ptr)) << std::endl;
}

struct base_a {};
struct base_b {};
struct derived : base_a, base_b {};

int main() {
  derived d;

  // http://en.cppreference.com/w/cpp/language/implicit_conversion
  // A prvalue pointer to a (optionally cv-qualified) derived class type can be
  // converted to a prvalue pointer to its (identically cv-qualified) base
  // class.
  base_b *b = &d; // implicit cast

  // undo implicit cast with static_cast
  derived *x = static_cast<derived *>(b);

  // reinterpret the value with reinterpret_cast
  derived *y = reinterpret_cast<derived *>(b);

  print_pointer(&b);
  print_pointer(&d);
  print_pointer(x);
  print_pointer(y);
}
