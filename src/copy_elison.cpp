// http://en.cppreference.com/w/cpp/language/copy_elision

#include <iostream>
#include <vector>

struct Noisy {
  Noisy() { std::cout << "constructed" << std::endl; }
  Noisy(const Noisy &) { std::cout << "copy-constructed" << std::endl; }
  Noisy(Noisy &&) { std::cout << "move-constructed" << std::endl; }
  ~Noisy() { std::cout << "destructed" << std::endl; }
};

std::vector<Noisy> f() {
  // copy elision when initializing v from a temporary (guaranteed in C++17)
  std::vector<Noisy> v = std::vector<Noisy>(3);

  // NRVO from v to the returned nameless temporary (not guaranteed in C++17)
  return v;
} // or the move constructor is called if optimizations are disabled

void g(std::vector<Noisy> arg) {
  std::cout << "arg.size() = " << arg.size() << std::endl;
}

int main() {
  // copy elision in initialization of v
  // from the result of f() (guaranteed in C++17)
  std::vector<Noisy> v = f();

  std::cout << std::endl;

  // copy elision in initialization of the parameter of g() from the result of
  // f() (guaranteed in C++17)
  g(f());
}
