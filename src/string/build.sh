#!/bin/sh

set -x
g++ StringEager.cc main.cc -Wall -m64 -o e64.out && ./e64.out
g++ StringEager.cc main.cc -Wall -m32 -o e32.out && ./e32.out
g++ StringEager.cc main.cc -Wall -m64 -std=c++0x -o f64.out && ./f64.out
g++ StringEager.cc main.cc -Wall -m32 -std=c++0x -o f32.out && ./f32.out

g++ StringEager.cc test.cc -Wall -DBOOST_TEST_DYN_LINK -lboost_unit_test_framework -m64 -o w64.out && ./w64.out
g++ StringEager.cc test.cc -Wall -m32 -o w32.out && ./w32.out
g++ StringEager.cc test.cc -Wall -DBOOST_TEST_DYN_LINK -lboost_unit_test_framework -m64 -std=c++0x -o u64.out && ./u64.out
g++ StringEager.cc test.cc -Wall -m32 -std=c++0x -o u32.out && ./u32.out

