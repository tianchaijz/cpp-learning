// http://en.cppreference.com/w/cpp/language/reinterpret_cast
// Unlike static_cast, but like const_cast, the reinterpret_cast expression does
// not compile to any CPU instructions. It is purely a compiler directive which
// instructs the compiler to treat the sequence of bits (object representation)
// of expression as if it had the type new_type.

#include <cassert>
#include <cstdint>
#include <iostream>

int f() { return 42; }

int main() {
  int i = 7;

  // pointer to integer and back
  uintptr_t v1 = reinterpret_cast<uintptr_t>(&i); // static_cast is an error
  std::cout << "The value of &i is 0x" << std::hex << v1 << std::endl;
  int *p1 = reinterpret_cast<int *>(v1);
  assert(p1 == &i);

  // pointer to function to another and back
  void (*fp1)() = reinterpret_cast<void (*)()>(f);
  // fp1(); undefined behavior
  int (*fp2)() = reinterpret_cast<int (*)()>(fp1);
  std::cout << std::dec << fp2() << std::endl; // safe

  // type aliasing through pointer
  char *p2 = reinterpret_cast<char *>(&i);
  if (p2[0] == '\x7')
    std::cout << "This system is little-endian" << std::endl;
  else
    std::cout << "This system is big-endian" << std::endl;

  // type aliasing through reference
  reinterpret_cast<unsigned int &>(i) = 42;
  std::cout << i << std::endl;
}
