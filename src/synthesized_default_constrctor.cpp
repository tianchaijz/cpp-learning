// https://groups.google.com/forum/#!topic/comp.lang.c++.moderated/2RtchRfH1vA
// http://stackoverflow.com/questions/2218254/variable-initialization-in-c
// http://stackoverflow.com/questions/2144012/explicit-type-conversion-and-multiple-simple-type-specifiers
// According to C++03 (5.2.3/2, expr.type.conv):
// "The expression T(), where T is a simple-type-specifier (7.1.5.2) for
// a non-array complete object type or the (possibly cv-qualified) void
// type, creates an rvalue of the specified type, which is value-
// initialized", while "unsigned int()" is not well-formed.
// To force initialization of a POD (which int is), you can use copy
// initializer syntax:
// int i = int();
// int j = int(1);

// http://stackoverflow.com/questions/906734/does-c-do-value-initialization-of-a-pod-typedef
// For a type T, T() value-initializes an "object" of type T and yields an
// rvalue expression.
//
// int a = int();
// assert(a == 0);
//
// Same for pod-classes:
//
// struct A { int a; };
// assert(A().a == 0);
//
// Also true for some non-POD classes that have no user declared constructor:
//
// struct A { ~A() { } int a; };
// assert(A().a == 0);
//
// Adding () propagates initializer calls to all POD members.

// struct A { ~A() { } int a; }; is a non-pod type. Had you introduced a
// constructor yourself, then the value of "a" depends on what that constructor
// does, of course.

// http://en.cppreference.com/w/cpp/language/declarations
// simple type specifier

// http://stackoverflow.com/questions/2584213/how-to-default-initialize-local-variables-of-built-in-types-in-c
// T x;
// x.~T();
// new (&x) T();

#include <iostream>

struct D {
  int random;
};

struct C {
  int num = 3;
};

// d is value initialized(D has no constructor)
// num is also value initialized
struct B {
  B() : num(), d() {}
  int num;
  int random;
  D d;
};

struct A {
  B b;
  C c;
  int num;
  int random;

  ~A() { std::cout << "A destructed" << std::endl; }
};

// http://en.cppreference.com/w/cpp/language/default_constructor
struct F {
  int &ref;    // reference member
  const int c; // const member
               // F::F() is implicitly defined as deleted
};

void value_initialized() {
  std::cout << "Call value_initialized" << std::endl;
  A a = A();
  std::cout << a.random << std::endl;
}

void default_constructor() {
  std::cout << "Call default_constructor" << std::endl;
  A a;
  std::cout << a.random << std::endl;
}

int main(int argc, char *argv[]) {
  // default constructor is called certainly
  A a;
  B b;

  int i = int();
  int j = int(1);

  // Classes that have members of built-in or compound type usually should rely
  // on the synthesized default constructor only if all such members have
  // in-class initializers.

  // A compound type is a type that is defined in terms of another type. C++ has
  // several compound types: reference, pointer, enum ...

  std::cout << i << std::endl;
  std::cout << j << std::endl;

  std::cout << a.random << std::endl;

  std::cout << (a.b.num == 0 ? "true" : "false") << std::endl;
  std::cout << a.b.random << std::endl;

  std::cout << (a.c.num == 3 ? "true" : "false") << std::endl;

  std::cout << "b.num == 0" << std::endl;
  std::cout << b.num << std::endl;

  std::cout << "b.d.random == 0" << std::endl;
  std::cout << b.d.random << std::endl;

  value_initialized();
  default_constructor();

  return 0;
}
